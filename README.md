# Backcasting Code - Summary
*Updated: February 19, 2022*


This repository contains four types of documents:

1. Several Jupyter notebooks that reproduce the main results in the manuscript
2. Two Python files that contain the bulk of the computation for backcasting and uncertainty quantification. They are used in some of the Jupyter notebooks.
3. The COVID dataset compiled by and available through OWID as it appeared on April 4, 2021.
4. [The manuscript](https://arxiv.org/abs/2202.00507), in the form available on Arxiv as of January 2022.

## Jupyter notebooks
1. ```seasonal_structure.ipynb``` extracts and plots confirmed cases and deaths due to COVID-19 over the period covered by the dataset. (Section 2)
2. ```backcasting_Italy.ipynb``` applies the backcasting algorithm described in the manuscript with only the GP error estimation to the Italy dataset, and plots the results. (Section 3)
3. ```numerical_results.ipynb``` performs backcasting using  death and hospitalization data (where available) as predictors, and produces the corresponding plots for several countries. (Section 3)
4. ```heuristics.ipynb``` specifically for the Italy dataset, computes and plots the effect of changing T*, and of using different embedding dimensions (time-windows) to perform backcasting. (Section 3)
5. ```uncertainty_estimates.ipynb``` implements the CRC daily case uncertainty estimates of Bohning et al, and uses that to perform backcasting on the Italy dataset based on the GP method developed in the manuscript. The code only shows the result using deaths as a predictor, but can easily be modified to show the result for hospitalizations. (Section 3)

## Python Files
1. ```backcaster.py``` defines a dataset class that can be used to perform backcasting in a few steps as described in the manuscript. It makes use of Python's ```sklearn``` Gaussian Process Regression implementation along with daily uncertainty estimates for case incidence based on [Bohning et. al.](https://www.sciencedirect.com/science/article/pii/S1201971220304446) referenced in the manuscript.
2. ```backcastingvis.py``` works with the backcaster class to provide quick plotting tools, though they are not comprehensive.

## Dataset

The dataset in the version appearing here (downloaded on April 4, 2021) is necessary to reproduce the results depicted in the manuscript. It is available through Our-World-In-Data [available on github](https://github.com/owid/covid-19-data/tree/master/public/data).

The corresponding license is copied below:

__License__

All visualizations, data, and code produced by _Our World in Data_ are completely open access under the [Creative Commons BY license](https://creativecommons.org/licenses/by/4.0/). You have the permission to use, distribute, and reproduce these in any medium, provided the source and authors are credited.

In the case of our vaccination dataset, please give the following citation:
> Mathieu, E., Ritchie, H., Ortiz-Ospina, E. _et al._ A global database of COVID-19 vaccinations. _Nat Hum Behav_ (2021). [https://doi.org/10.1038/s41562-021-01122-8](https://doi.org/10.1038/s41562-021-01122-8)

In the case of our testing dataset, please give the following citation:
> Hasell, J., Mathieu, E., Beltekian, D. _et al._ A cross-country database of COVID-19 testing. _Sci Data_ **7**, 345 (2020). [https://doi.org/10.1038/s41597-020-00688-8](https://doi.org/10.1038/s41597-020-00688-8)

The data produced by third parties and made available by _Our World in Data_ is subject to the license terms from the original third-party authors. We will always indicate the original source of the data in our database, and you should always check the license of any such third-party data before use.
