# make relevant imports
import numpy as np
from sklearn import gaussian_process
from sklearn.gaussian_process.kernels import Matern, WhiteKernel, RBF
from sklearn.gaussian_process.kernels import ConstantKernel, RationalQuadratic


class BackCaster:
    def __init__(self, x, y, t_star, window, norm=True):
        self.norm = norm  # normalize, boolean
        self.x_orig, self.y_orig = x, y  # original dataset
        self.x, self.y = self.__normalization(x, y)  # normalized dataset
        self.t_star = t_star  # bakccasting date
        self.window = window  # embedding dimension / time window
        self.targets, self.predictors = self.__create_tuples()
        self.pred = None  # predictions
        self.sigma_pred = None  # predicted intervals

    def __create_tuples(self):
        '''
        create sliding window tuples of single time series
        '''
        targets = []
        for i in range(self.window, len(self.y)):
            targets.append(self.y[i])

        predictors = []
        for i in range(0, len(self.x)-self.window):
            temp = []
            for j in range(self.window):
                temp.append(self.x[i+j])
            predictors.append(temp)
        # convert to numpy
        targets, predictors = np.array(targets), np.array(predictors).squeeze()
        return targets, predictors

    def __normalization(self, x, y):
        '''
        normalize to mean 0, stdev 1
        '''
        if self.norm:
            x = (x - x.mean())/x.std()
            y = (y - y.mean())/y.std()
        return x, y

    def fit_gp(self):
        '''
        perform GP fit
        '''
        # to be changed later
        kernel = (ConstantKernel() +
                  Matern(length_scale=10, nu=7/2,
                  length_scale_bounds=(10, 30)) + WhiteKernel())
        gp = gaussian_process.GaussianProcessRegressor(kernel=kernel)
        gp.fit(self.predictors[:][self.t_star:],
               self.targets[:][self.t_star:])
        pred, sigma_pred = gp.predict(self.predictors, return_std=True)
        if self.norm:
            pred = pred * self.y_orig.std() + self.y_orig.mean()
            sigma_pred = sigma_pred * self.y_orig.std()
        self.pred = pred
        self.sigma_pred = sigma_pred
        return


def generate_CRC(cases, deaths):
    '''
    Compute Capture-Recapture uncertainty around cases
    (based onBoening et al.)
    '''
    # define capture_recapture function
    T_end = len(cases)

    CRC_cases = np.zeros(len(cases)-1)

    CRC_error = np.zeros(len(cases)-1)

    # compute CRC estimate for cases
    for i in range(1, T_end-1):
        if (cases[i-1]-deaths[i] == 0):
            CRC_cases[i] = 0
        else:
            CRC_cases[i] = cases[i]**2 / (cases[i-1]-deaths[i])

    # compute CRC error estimate
    for i in range(1, T_end-1):
        k1 = cases[i]**4 / (1+cases[i-1]-deaths[i])**3
        k2 = 4 * cases[i]**3 / (1+cases[i-1]-deaths[i])**2
        k3 = cases[i]**2 / (1+cases[i-1]-deaths[i])
        CRC_error[i] = max(k1 + k2 + k3, 0)
    CRC_error_cumulative = CRC_error.cumsum()

    # CRC_error is stdev!
    return CRC_cases, CRC_error, CRC_error_cumulative

    # sigma = 1.96 * np.sqrt(CRC_error_cumulative)
