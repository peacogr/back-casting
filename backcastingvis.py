# make relevant imports
import numpy as np
import matplotlib.pyplot as plt


def errorplot(y, sigma_y, t_star, window, color='violet'):
    '''
    Plot daily predictions and time series along with 95% uncertainty interval
    '''
    plt.figure(figsize=(11, 6))
    plt.errorbar(np.arange(window, window+len(y)), y, yerr=1.96*sigma_y,
                 color=color, ecolor='purple', mec='r',
                 label=r'Back-Casting + $95\%$ Error')
    plt.ylabel('Number of Cases')
    plt.xlabel('Days since Jan. 1, 2020')
    return


def errorplot_cum(y, sigma_y, t_star, window, color='violet'):
    '''
    Plot cumulative time series prediction along with 95% confidence intervals
    The upper and lower uncertainty time series are computed based on
    sigma_y: estimated daily stdev
    '''
    plt.figure(figsize=(11, 6))
    plt.plot(np.arange(window, len(y)+window), y.cumsum(), c=color,
             label='Predicted Cases')
    plt.plot(np.arange(window, len(y)+window),
             y.cumsum()+1.96*sigma_y.cumsum(), '--', c='purple',
             linewidth=3, label=r'$>95\%$ CI')

    a = y - 1.96*sigma_y
    a[a < 0] = 0
    plt.plot(np.arange(window, len(y)+window),
             a.cumsum(), '--', c='purple', linewidth=3)

    plt.ylabel('Number of Cases')
    plt.xlabel('Days since Jan. 1, 2020')
    return


def specified_bounds(y, y_up, y_down, t_star, window):
    '''
    Plot cumulative time series prediction along with 95% confidence intervals
    The upper and lower uncertainty time series are pre-specified as arguments
    '''
    plt.figure(figsize=(11, 6))
    plt.plot(np.arange(window, window+len(y)), y.cumsum(), color='orange')
    plt.plot(np.arange(window, window+len(y)), y_up.cumsum(), '--', c='navy',
             linewidth=3, label=r'>95\%  CI')
    plt.plot(np.arange(window, window+len(y)), y_down.cumsum(), '--', c='navy',
             linewidth=3)
    plt.ylabel('Number of Cases')
    plt.xlabel('Days since Jan. 1, 2020')
    return
